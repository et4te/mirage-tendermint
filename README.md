# mirage-tendermint

An implementation of Tendermint using MirageOS, OCaml and PSS based gossip.

## Dependencies

The nix package manager is required in order to build the source code reproducibly. 

Install nix and create the following `/etc/nix/nix.conf`.

```conf
sandbox = false

substituters = https://cache.nixos.org https://nixcache.reflex-frp.org

trusted-public-keys = cache.nixos.org-1:6NCHdD59X431o0gWypbMrAURkbJ16ZPMQFGspcDShjY= ryantrinkle.com-1:JJiAKaRv9mWgpVAz8dwewnZe0AzzEAzPkagE9SP5NWI=
```

This will ensure that dependencies are downloaded from cached builds and substantially
speed up compilation times.

## Building

To build mirage-tendermint, use the following command.

```bash
$ nix-build
```

This will generate binaries in `./result/bin`.

## Running the experiment

The experiment is comprised of 7 nodes. Each node is configured via a settings file in
`./src/priv/` and operate via a block schedule in `./src/priv/schedule.sexp` where the
index is the height and the public key corresponds to the proposer at that height.

```
# Run all nodes
$ result/bin/mirage-tendermint --name=a
$ result/bin/mirage-tendermint --name=b
$ result/bin/mirage-tendermint --name=c
$ result/bin/mirage-tendermint --name=d
$ result/bin/mirage-tendermint --name=e
$ result/bin/mirage-tendermint --name=f
$ result/bin/mirage-tendermint --name=g
```

The gossip constants can be tweaked in each settings file and the tendermint timeouts
are in `./src/tendermint.ml`.

Once the nodes are running they should converge and reach consensus.