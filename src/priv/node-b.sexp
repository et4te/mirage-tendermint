((identity
  (Signed
   ((public_key
     8a76edb9c02c7af83ec3c8aa060c6827b92c8efeff9eac180539935587309d12)
    (address ((ip 0.0.0.0) (udp_port 4001) (tcp_port 3001))))
   a9798f50b0b1614ebc1498267e570541fab4b19ff9d3aabe9c79e83e0341f3413fc35517a9f9067d43e2d144c0141c677106a2f26d458f8a9061b0a61361b205))
 (secret_key
  110cfda4db66d1fd27f1878607d86fe31031a12c2e03cc51b87746aed1d04f3a)
 (bootstrap_peers
  ((Signed
    ((public_key
      1c1a4da91e5188ad3c50ec95b6b2d7caebc9c88aa6d772fba80e0e94e71801c9)
     (address ((ip 0.0.0.0) (udp_port 4002) (tcp_port 3002))))
    aedd8695a7d30621b11a2bc907ce600a985882aef47e4f0b29a5ec5187c55806b350237404b0dafe08de3891080af90e415c6ca5e3977c358b845f2f4622fb00)
   (Signed
    ((public_key
      a0f647622accf753c44c8f7e97887a4f115a0cba277516fa356ba797ff4c426b)
     (address ((ip 0.0.0.0) (udp_port 4004) (tcp_port 3004))))
    282168f9d66bec764052627635edc2ebd3642fbae160dce540c07df68bd0a93e4d338e193f5508e459fca850dda3f2573ee721ec31d8e7d5e664117caa51aa0f)))
 (refresh_period 1000000000) (round_trip_time 750000000))
