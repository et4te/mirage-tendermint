((identity
  (Signed
   ((public_key
     e81f98a6d5c10b43cc86f9aa6e92682cfa48b75775ddbc4df46a22efe9ae5e50)
    (address ((ip 0.0.0.0) (udp_port 4000) (tcp_port 3000))))
   5bf4e135f96ed76cb163ea35ea69dcc6ff6ee6c812a618ae884c54f2c062322610c9b7a4166a868a5064df34e61b9d4211f4bc617134ca207d4676c8c00c030c))
 (secret_key
  b113b750d62904777c41eb41fb1f7cba1da6aea758d981036c0b1c981c473eee)
 (bootstrap_peers
  ((Signed
    ((public_key
      1c1a4da91e5188ad3c50ec95b6b2d7caebc9c88aa6d772fba80e0e94e71801c9)
     (address ((ip 0.0.0.0) (udp_port 4002) (tcp_port 3002))))
    aedd8695a7d30621b11a2bc907ce600a985882aef47e4f0b29a5ec5187c55806b350237404b0dafe08de3891080af90e415c6ca5e3977c358b845f2f4622fb00)))
 (refresh_period 1000000000) (round_trip_time 750000000))