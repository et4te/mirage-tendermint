((identity
  (Signed
   ((public_key
     688fdda97b9e9aa23ce51b192fa7340f8128519b968728e8bfce62076d7c48cb)
    (address ((ip 0.0.0.0) (udp_port 4003) (tcp_port 3003))))
   01aa820f91b683363072a4edc0f30cae166c840bd52fa22e8bdbe427e70bced4336aa2fd9395d849fd3c80d4b009c4003c13077ec5681bcfd0d4c60585e2df03))
 (secret_key
  88161b1cc44da2e67edb23d6c7e37d2e58038db4b91db47958bcc4701bd3f6a1)
 (bootstrap_peers
  ((Signed
    ((public_key
      1c1a4da91e5188ad3c50ec95b6b2d7caebc9c88aa6d772fba80e0e94e71801c9)
     (address ((ip 0.0.0.0) (udp_port 4002) (tcp_port 3002))))
    aedd8695a7d30621b11a2bc907ce600a985882aef47e4f0b29a5ec5187c55806b350237404b0dafe08de3891080af90e415c6ca5e3977c358b845f2f4622fb00)))
 (refresh_period 1000000000) (round_trip_time 750000000))
