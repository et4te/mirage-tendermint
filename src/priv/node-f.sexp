((identity
  (Signed
   ((public_key
     7cf43fb634816fb9af21fabc1b50077be7b43ca5f70caede162d5fb7daeb1dde)
    (address ((ip 0.0.0.0) (udp_port 4005) (tcp_port 3005))))
   30bf73214b563b0691dbb08c0482dc212487faba66d89bddb8b1eaa6da3693dbbd8c2dddfca6957bda36a1206b4e86f6efb7ecec8c2f2abfa2654f27cf18d404))
 (secret_key
  475f6fc3af705c1ed4d10aaa7465911dd9292dfa2b27cde4535d8a19774d9491)
 (bootstrap_peers
  ((Signed
    ((public_key
      8a76edb9c02c7af83ec3c8aa060c6827b92c8efeff9eac180539935587309d12)
     (address ((ip 0.0.0.0) (udp_port 4001) (tcp_port 3001))))
    a9798f50b0b1614ebc1498267e570541fab4b19ff9d3aabe9c79e83e0341f3413fc35517a9f9067d43e2d144c0141c677106a2f26d458f8a9061b0a61361b205)))
 (refresh_period 1000000000) (round_trip_time 750000000))
