((identity
  (Signed
   ((public_key
     1c1a4da91e5188ad3c50ec95b6b2d7caebc9c88aa6d772fba80e0e94e71801c9)
    (address ((ip 0.0.0.0) (udp_port 4002) (tcp_port 3002))))
   aedd8695a7d30621b11a2bc907ce600a985882aef47e4f0b29a5ec5187c55806b350237404b0dafe08de3891080af90e415c6ca5e3977c358b845f2f4622fb00))
 (secret_key
  21725345f6dba2097d61c39a47fdcf99c58dbf07596e74811589b99d8e83096a)
 (bootstrap_peers
  ((Signed
    ((public_key
      7cf43fb634816fb9af21fabc1b50077be7b43ca5f70caede162d5fb7daeb1dde)
     (address ((ip 0.0.0.0) (udp_port 4005) (tcp_port 3005))))
    30bf73214b563b0691dbb08c0482dc212487faba66d89bddb8b1eaa6da3693dbbd8c2dddfca6957bda36a1206b4e86f6efb7ecec8c2f2abfa2654f27cf18d404)
   (Signed
    ((public_key
      a0f647622accf753c44c8f7e97887a4f115a0cba277516fa356ba797ff4c426b)
     (address ((ip 0.0.0.0) (udp_port 4004) (tcp_port 3004))))
    282168f9d66bec764052627635edc2ebd3642fbae160dce540c07df68bd0a93e4d338e193f5508e459fca850dda3f2573ee721ec31d8e7d5e664117caa51aa0f)
   (Signed
    ((public_key
      688fdda97b9e9aa23ce51b192fa7340f8128519b968728e8bfce62076d7c48cb)
     (address ((ip 0.0.0.0) (udp_port 4003) (tcp_port 3003))))
    01aa820f91b683363072a4edc0f30cae166c840bd52fa22e8bdbe427e70bced4336aa2fd9395d849fd3c80d4b009c4003c13077ec5681bcfd0d4c60585e2df03)))
 (refresh_period 1000000000) (round_trip_time 750000000))
