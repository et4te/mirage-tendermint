((identity
  (Signed
   ((public_key
     ba7a33be40eb90087da2c7321796af83c8e73ec34ac2450c3e2d1a6c7f57fb48)
    (address ((ip 0.0.0.0) (udp_port 4006) (tcp_port 3006))))
   ba0b312e140b37b72f0d3e5ba223a190d7276ccb99f729c515e0ca7e494872c0a93e12c86eabe350a33ab75541aeeb07f4a34f1f37103b2a561858859ce6ad07))
 (secret_key
  cb396673158696002d2779fc14570a6a8ef36cbabd76ec5c309559a4754bb361)
 (bootstrap_peers
  ((Signed
    ((public_key
      a0f647622accf753c44c8f7e97887a4f115a0cba277516fa356ba797ff4c426b)
     (address ((ip 0.0.0.0) (udp_port 4004) (tcp_port 3004))))
    282168f9d66bec764052627635edc2ebd3642fbae160dce540c07df68bd0a93e4d338e193f5508e459fca850dda3f2573ee721ec31d8e7d5e664117caa51aa0f)))
 (refresh_period 1000000000) (round_trip_time 750000000))
