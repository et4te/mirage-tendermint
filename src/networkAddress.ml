open Core_kernel

module Make (S : Mirage_stack_lwt.V4) = struct

  (* Extension to the network stack to make ip addresses serialize / deserialize. *)
  module S = Stack_ext.Make(S)

  type t = {
    ip : S.ipv4addr;
    udp_port : int;
    tcp_port : int;
  } [@@deriving bin_io, sexp]

  let create ip udp_port tcp_port =
    { ip; udp_port; tcp_port; }

end
