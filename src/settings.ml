open Core_kernel
open Lwt

module Make
         (C : Mirage_console_lwt.S)
         (S : Mirage_stack_lwt.V4)
         (KV : Mirage_kv_lwt.RO)
  = struct

  module Logging = Logging.Make(C)

  module Identity = Identity.Make(S)

  type t = {
      identity : Identity.s;
      secret_key : Crypto.secret_key;
      bootstrap_peers : Identity.s list;
      refresh_period : int;
      round_trip_time : int;
    } [@@deriving bin_io, sexp]

  let create identity secret_key bootstrap_peers refresh_period round_trip_time =
    { identity; secret_key; bootstrap_peers; refresh_period; round_trip_time }

  (* Accessors *)

  let get_identity t =
    t.identity

  let get_identity_exn t =
    Identity.of_signed_exn t.identity

  let get_round_trip_time t =
    t.round_trip_time

  (* Serialize / Deserialize *)

  let serialize t =
    t |> sexp_of_t |> Sexp.to_string_hum

  let deserialize s =
    s |> Sexp.of_string |> t_of_sexp

  let of_cstruct cs =
    cs |> Cstruct.concat |> Cstruct.to_string |> deserialize

  exception No_bootstrap_peers

  let random_bootstrap_id t =
    let n_bootstrap_peers = List.length t.bootstrap_peers in
    if n_bootstrap_peers > 0 then
      begin
        let i = Random.int n_bootstrap_peers in
        let p = List.nth_exn t.bootstrap_peers i in
        Identity.of_signed_exn p
      end
    else
      raise No_bootstrap_peers

end
