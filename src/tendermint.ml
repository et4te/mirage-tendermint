open Core_kernel
open Lwt
open Gossip

module Make
         (C : Mirage_console_lwt.S)
         (T : Mirage_time_lwt.S)
         (Pclock : Mirage_types.PCLOCK)
         (KV : Mirage_kv_lwt.RO)
  = struct

  module Logging = Logging.Make(C)

  module ProposalSchedule = ProposalSchedule.Make(C)(KV)

  (* Constants - Assuming 7 nodes *)

  let n = 7
  let f = n / 3
  let prevote_threshold = 2 * f + 1
  let precommit_threshold = 2 + f + 1
  let superior_rounds_threshold = f + 1
  let timeout_propose_multiplier = 500000000 (* 0.5s *)
  let timeout_prevote_multiplier = 500000000
  let timeout_precommit_multiplier = 500000000
  let sync_repeat_threshold = 10 * 9
  let endorsement_cutoff = 10             (* after 10 rounds, forget endorsements *)

  (* Custom *)

  let superior_height_threshold = 4

  (* Types *)

  type step = ProposeStep | PrevoteStep | PrecommitStep
    [@@deriving bin_io, sexp]

  module GossipElement = struct
    type t = Gossip.t [@@deriving bin_io, sexp]
    let compare = Pervasives.compare
    let make x = x
  end

  module GossipSet = Set.Make(GossipElement)

  type t = {
      console : C.t;
      pclock : Pclock.t;
      public_key : Crypto.public_key;
      secret_key : Crypto.secret_key;
      consensus_agent : ConsensusAgent.t;
      sync_agent : SyncAgent.t;
      mutable height : int;
      mutable round : int;
      mutable step : step;
      decisions : (int, Block.t) Hashtbl.Poly.t;
      mutable locked_value : Block.t option;
      mutable locked_round : int;
      mutable valid_value : Block.t option;
      mutable valid_round : int;
      mutable gossip : GossipSet.t;
      mutable first_prevote : bool;
      mutable first_proposal_lock : bool;
      mutable first_precommit : bool;
      mutable first_sync : bool;
      mutable sync_repeat : int;
      mutable in_sync : bool;
      mutable previous_block : Block.t;
      schedule : ProposalSchedule.t;
    }

  let create c pclock pk sk schedule genesis = {
      console = c;
      pclock;
      public_key = pk;
      secret_key = sk;
      consensus_agent = ConsensusAgent.create ();
      sync_agent = SyncAgent.create ();
      height = 0;
      round = 0;
      step = ProposeStep;
      decisions = Hashtbl.Poly.create ();
      locked_value = None;
      locked_round = -1;
      valid_value = None;
      valid_round = -1;
      gossip = GossipSet.empty;
      first_prevote = true;
      first_proposal_lock = true;
      first_precommit = true;
      first_sync = true;
      in_sync = true;
      sync_repeat = 0;
      previous_block = genesis;
      schedule;
    }

  let get_state t =
    match SyncAgent.status t.sync_agent with
    | [] ->
       ConsensusAgent.get_state t.consensus_agent;
    | Some (pk, _, _)::_ ->
       if t.public_key = pk then
         ConsensusAgent.get_state t.consensus_agent
       else
         if t.in_sync then
           SyncAgent.get_state t.sync_agent
         else
           ConsensusAgent.get_state t.consensus_agent

  let broadcast t gossip =
    ConsensusAgent.broadcast t.consensus_agent gossip;
    SyncAgent.store t.sync_agent t.height gossip;
    ()

  let proposer t h r =
    ProposalSchedule.proposer t.schedule h r

  let valid t p block =
    Block.valid p t.previous_block block

  let hash = function
    | Some block ->
       Block.hash block
    | None ->
       raise Block.Invalid_block

  let sign sk h =
    Crypto.sign sk h

  let verify pk h signature =
    match h with
    | Some h ->
       (match signature with 
        | Some signature ->
           Crypto.verify pk h signature
        | None ->
           false)
    | None ->
       false

  (* Timeouts *)

  let rec soft_exp x =
    if x <= 2 then
      1
    else
      soft_exp (x - 2) + soft_exp (x - 3)

  let memo_get_soft_exp arr i =
    match arr.(i) with
    | None ->
       let x = soft_exp i in
       arr.(i) <- Some (soft_exp i);
       x
    | Some x ->
       x

  let precalc = Array.init 40 ~f:(fun x -> Some (soft_exp x))

  let memo_exp x =
    if x <= 2 then
      memo_get_soft_exp precalc x
    else
      memo_get_soft_exp precalc (x - 2) + memo_get_soft_exp precalc (x - 3)

  let timeout_propose r =
    Int64.of_int (memo_exp r * timeout_propose_multiplier)

  let on_timeout_propose t h r =
    if t.height = h && t.round = r && t.step = ProposeStep then
      begin
        let () = broadcast t (Prevote (t.public_key, t.height, t.round, None, None)) in
        t.step <- PrevoteStep;
        return ()
      end
    else
      return ()

  let timeout_prevote r =
    Int64.of_int (memo_exp r * timeout_prevote_multiplier)

  let on_timeout_prevote t h r =
    if t.height = h && t.round = r && t.step = PrevoteStep then
      begin
        let () = broadcast t (Precommit (t.public_key, t.height, t.round, None, None)) in
        t.step <- PrecommitStep;
        return ()
      end
    else
      return ()

  (* Start *)

  let start_round t round =
    t.first_prevote <- true;
    t.first_proposal_lock <- true;
    t.first_precommit <- true;
    t.round <- round;
    t.step <- ProposeStep;
    if proposer t t.height t.round = t.public_key then
      match t.valid_value with
      | Some _ ->
         let ps = Crypto.public_key_to_string_hum t.public_key in       
         let s = Printf.sprintf "[%s] Broadcasting existing block" ps in
         ignore(Logging.print t.console s);
         broadcast t (Proposal (t.public_key, t.height, t.round, t.valid_value, t.valid_round))
      | None ->
         let ps = Crypto.public_key_to_string_hum t.public_key in       
         let s = Printf.sprintf "[%s] Broadcasting generated block" ps in
         ignore(Logging.print t.console s);
         let (d, ps) = Pclock.now_d_ps t.pclock in
         let proposal = Block.generate_valid t.public_key t.previous_block [] (d, ps) in
         broadcast t (Proposal (t.public_key, t.height, t.round, Some proposal, t.valid_round))
    else
      (* We are not the proposer, transition to PrevoteStep *)
      async(fun () ->
          T.sleep_ns (timeout_propose t.round) >>= fun () ->
          on_timeout_propose t t.height t.round >>= fun () ->
          return ()
        )

  let start t =
    start_round t 0

  (* Timeouts dependent on start *)

  let timeout_precommit r =
    Int64.of_int (memo_exp r * timeout_precommit_multiplier)

  let on_timeout_precommit t h r =
    if t.height = h && t.round = r then
      begin
        start_round t (t.round + 1);
        return ()
      end
    else
      return ()

  (* State updates *)

  (* L22 *)

  let upon_proposal_1 t =
    let rec loop gossip = match gossip with
      | [] ->
         false
      | Proposal (p, h, r, v, vr) :: tl ->
         if t.step = ProposeStep then
           if proposer t t.height t.round = p then
             if vr = -1 && t.height = h && t.round = r then
               begin
                 if (valid t p v) && (t.locked_round = -1 || t.locked_value = v) then
                   if t.round >= endorsement_cutoff then
                     broadcast t (Prevote (t.public_key, t.height, t.round, Some (hash v), None))
                   else
                     begin
                       let hv = hash v in
                       let sg = sign t.secret_key hv in
                       broadcast t (Prevote (t.public_key, t.height, t.round, Some hv, Some sg))
                     end
                 else
                   broadcast t (Prevote (t.public_key, t.height, t.round, None, None));
                 t.step <- PrevoteStep;
                 true
               end
             else
               loop tl
           else
             loop tl
         else
           loop tl
      | _::tl ->
         loop tl
    in
    loop (GossipSet.to_list t.gossip)

  (* L28 *)

  let upon_valid_prevotes_1 t vr v =
    let rec loop gossip threshold = match gossip with
      | [] ->
         false
      | Prevote (p, h, r, id, s) ::tl ->
         if (h = t.height) && (r = vr) && id = (Some (hash v)) then
           if r >= endorsement_cutoff then
             (* 2f + 1 valid prevotes *)
             if threshold + 1 >= prevote_threshold then
               true
             else
               loop tl (threshold + 1)
           else
             (* verify endorsement *)
             if verify p id s then
               if threshold + 1 >= prevote_threshold then
                 true
               else
                 loop tl (threshold + 1)
             else (* require a signature < endorsement cutoff *)
               loop tl threshold
         else
           loop tl threshold
      | _ :: tl ->
         loop tl threshold
    in
    loop (GossipSet.to_list t.gossip) 0

  let upon_proposal_2 t =
    let rec loop gossip = match gossip with
      | [] ->
         false
      | Proposal (p, h, r, v, vr) :: tl ->
         if t.height = h && t.round = r then
           if proposer t t.height t.round = p then
             if upon_valid_prevotes_1 t vr v then
               if t.step = ProposeStep && (vr >= 0 && vr < t.round) then
                 begin
                   if (valid t p v) && (t.locked_round <= vr || t.locked_value = v) then
                     (* if round >= endorsement_cutoff, no need to endorse *)
                     if t.round >= endorsement_cutoff then
                       broadcast t (Prevote (t.public_key, t.height, t.round, Some (hash v), None))
                     else
                       begin
                         let hv = hash v in
                         let sg = sign t.secret_key hv in
                         broadcast t (Prevote (t.public_key, t.height, t.round, Some hv, Some sg))
                       end
                   else
                     broadcast t (Prevote (t.public_key, t.height, t.round, None, None));
                   t.step <- PrevoteStep;
                   true
                 end
               else
                 loop tl
             else
               loop tl
           else
             loop tl
         else
           loop tl
      | hd::tl ->
         loop tl
    in
    loop (GossipSet.to_list t.gossip)

  (* L34 *)

  let upon_prevote_once t =
    let rec loop gossip threshold = match gossip with
      | [] ->
         false
      | Prevote (p, h, r, _, _) :: tl ->
         if t.step = PrevoteStep then
           if t.height = h && t.round = r then
             if threshold + 1 >= prevote_threshold then
               begin
                 t.first_prevote <- false;
                 async(fun () ->
                     T.sleep_ns (timeout_prevote t.round) >>= fun () ->
                     on_timeout_prevote t t.height t.round >>= fun () ->
                     return ()
                   );
                 true
               end
             else
               loop tl (threshold + 1)
           else
             loop tl threshold
         else
           loop tl threshold
      | _ :: tl ->
         loop tl threshold
    in
    if t.first_prevote then
      loop (GossipSet.to_list t.gossip) 0
    else
      false

  (* L36 *)

  let print_step t =
    let ss = t.step |> sexp_of_step |> Sexp.to_string_hum in
    ignore(Logging.print t.console (Printf.sprintf "Step = %s" ss))

  let upon_valid_prevotes_2 t h1 r1 v =
    let rec loop gossip threshold = match gossip with
      | [] ->
         false
      | Prevote (p, h, r, id, s) :: tl ->
         if h = h1 && r = r1 && id = (Some (hash v)) then
           if r >= endorsement_cutoff then
             (* 2f + 1 valid prevotes *)
             if threshold + 1 >= prevote_threshold then
               true
             else
               loop tl (threshold + 1)
           else
             if verify p id s then
               if threshold + 1 >= prevote_threshold then
                 true
               else
                 loop tl (threshold + 1)
             else
               loop tl threshold
         else
           loop tl threshold
      | _ :: tl ->
         loop tl threshold
    in
    loop (GossipSet.to_list t.gossip) 0

  let upon_proposal_and_prevote t =
    let rec loop gossip = match gossip with
      | [] ->
         false
      | Proposal (p, h, r, v, vr) :: tl ->
         if t.height = h && t.round = r then
           if proposer t t.height t.round = p then
             if (valid t p v) && ((t.step = PrevoteStep) || (t.step = PrecommitStep)) then
               if upon_valid_prevotes_2 t t.height t.round v then
                 begin
                   (* print_step t; *)
                   t.first_proposal_lock <- false;
                   if t.step = PrevoteStep then
                     begin
                       t.locked_value <- v;
                       t.locked_round <- t.round;
                       let hv = hash v in
                       let sg = sign t.secret_key hv in
                       broadcast t (Precommit (t.public_key, h, r, Some hv, Some sg));
                       t.step <- PrecommitStep;
                     end;
                   t.valid_value <- v;
                   t.valid_round <- r;
                   true
                 end
               else
                 loop tl
             else
               loop tl
           else
             loop tl
         else
           loop tl
      | _ :: tl ->
         loop tl
    in
    if t.first_proposal_lock then
      loop (GossipSet.to_list t.gossip)
    else
      false

  (* L44 *)

  let upon_nil_prevotes t =
    let rec loop gossip threshold = match gossip with
      | [] ->
         false
      | Prevote (p, h, r, None, s) :: tl ->
         if h = t.height && r = t.round then
           (* 2f + 1 nil prevotes *)
           if threshold + 1 >= prevote_threshold then
             begin
               broadcast t (Precommit (t.public_key, t.height, t.round, None, None));
               t.step <- PrecommitStep;
               true
             end
           else
             loop tl (threshold + 1)
         else
           loop tl threshold
      | _ :: tl ->
         loop tl threshold
    in
    if t.step = PrevoteStep then
      loop (GossipSet.to_list t.gossip) 0
    else
      false

  (* L47 *)

  let upon_precommit_once t =
    let rec loop gossip threshold = match gossip with
      | [] ->
         false
      | Precommit (p, h, r, _, _) :: tl ->
         if h = t.height && r = t.round then
           (* 2f + 1 precommits for the first time *)
           if threshold + 1 >= precommit_threshold then
             begin
               t.first_precommit <- false;
               async(fun () ->
                   T.sleep_ns (timeout_precommit t.round) >>= fun () ->
                   on_timeout_precommit t t.height t.round >>= fun () ->
                   return ()
                 );
               true
             end
           else
             loop tl (threshold + 1)
         else
           loop tl threshold
      | _ :: tl ->
         loop tl threshold
    in
    if t.first_precommit then
      loop (GossipSet.to_list t.gossip) 0
    else
      false

  (* L49 *)

  (* For each proposal at t.height with 2f + 1 valid precommits whilst
       decision[t.height] = None, trigger
   *)

  let print_convergence t b =
    let s = Block.serialize b in
    Logging.print t.console (Printf.sprintf "---\nConsensus converged on block\n%s\n---" s)

  let upon_valid_precommits t h1 r1 id1 =
    let rec loop gossip threshold = match gossip with
      | [] ->
         false
      | Precommit (p, h, r, id, s) :: tl ->
         if h = h1 && r = r1 && id = id1 then
           if r >= endorsement_cutoff then
             (* 2f + 1 valid precommits *)
             if threshold + 1 >= precommit_threshold then
               true
             else
               loop tl (threshold + 1)
           else
             (* verify endorsement *)
             if verify p id s then
               if threshold + 1 >= precommit_threshold then
                 true
               else
                 loop tl (threshold + 1)
             else
               loop tl threshold
         else
           loop tl threshold
      | _ :: tl ->
         loop tl threshold
    in
    loop (GossipSet.to_list t.gossip) 0

  let upon_proposal_and_precommit t =
    let older gossip new_height =
      match gossip with
      | Proposal (_, h, _, _, _)
      | Prevote (_, h, _, _, _)
      | Precommit (_, h, _, _, _) ->
         h >= new_height
    in
    let rec loop gossip = match gossip with
      | [] ->
         false
      | Proposal (p, h, r, v, _) :: tl ->
         if proposer t t.height r = p then
           if h = t.height then
             if upon_valid_precommits t t.height r (Some (hash v)) then
               if Hashtbl.find t.decisions h = None then
                 if valid t p v then
                   begin
                     ignore(print_convergence t (Block.get_block_exn v));
                     let _ = Hashtbl.add t.decisions h (Block.get_block_exn v) in
                     t.height <- t.height + 1;
                     t.locked_value <- None;
                     t.locked_round <- -1;
                     t.valid_value <- None;
                     t.valid_round <- -1;
                     t.gossip <- GossipSet.filter t.gossip ~f:(fun g -> older g t.height);
                     t.first_prevote <- true;
                     t.first_proposal_lock <- true;
                     t.first_precommit <- true;
                     t.first_sync <- true;
                     t.in_sync <- true;
                     t.previous_block <- Block.get_block_exn v;
                     ignore(start_round t 0);
                     true
                   end
                 else
                   loop tl
               else
                 loop tl
             else
               loop tl
           else
             loop tl
         else
           loop tl
      | _::tl ->
         loop tl
    in
    loop (GossipSet.to_list t.gossip)

  (* L55 *)

  (* Counts the frequency of the max value of rounds greater than t.rounds *)

  let upon_superior_rounds t =
    let rec loop gossip saved = match gossip with
      | [] ->
         None
      | Proposal (_, h, r, _, _) :: tl
      | Prevote (_, h, r, _, _) :: tl
      | Precommit (_, h, r, _, _) :: tl ->
         if t.height = h then
           if r > t.round then
             (match Hashtbl.find saved r with
              | Some count ->
                 if (count + 1) >= superior_rounds_threshold then
                   (Some r)
                 else
                   begin
                     Hashtbl.set saved r (count + 1);
                     loop tl saved
                   end
              | None ->
                 let _ = Hashtbl.add saved r 1 in
                 loop tl saved)
           else
             loop tl saved
         else
           loop tl saved
      | _ :: tl ->
         loop tl saved
    in
    match loop (GossipSet.to_list t.gossip) (Hashtbl.Poly.create ()) with
    | None ->
       false
    | Some r ->
       start_round t r;
       true

  (* Custom *)

  (* Counts the frequency of the max value of height greater than t.height *)

  let upon_superior_height t =
    let rec loop gossip saved = match gossip with
      | [] ->
         None
      | Proposal (_, h, _, _, _) :: tl
      | Prevote (_, h, _, _, _) :: tl
      | Precommit (_, h, _, _, _) :: tl ->
         if h > t.height then
           (match Hashtbl.find saved h with
            | Some count ->
               if (count + 1) >= superior_height_threshold then
                 (Some h)
               else
                 begin
                   Hashtbl.set saved h (count + 1);
                   loop tl saved
                 end
            | None ->
               let _ = Hashtbl.set saved h 1 in
               loop tl saved)
         else
           loop tl saved
      | _ :: tl ->
         loop tl saved
    in
    match loop (GossipSet.to_list t.gossip) (Hashtbl.Poly.create ()) with
    | None ->
       false
    | Some h ->
       if t.first_sync then
         begin
           t.first_sync <- false;
           t.in_sync <- false;
           ignore(Logging.print t.console "------------- OUT-OF-SYNC ----------------");
           SyncAgent.activate t.sync_agent t.public_key t.height t.round;
           broadcast t (Sync (t.public_key, t.height, t.round));
           true
         end 
       else
         if t.sync_repeat >= sync_repeat_threshold then
           begin
             t.sync_repeat <- 0;
             broadcast t (Sync (t.public_key, t.height, t.round));
             true
           end
         else
           begin
             t.sync_repeat <- t.sync_repeat + 1;
             false
           end
      
  let trigger_rules t =
    (* L22 *)
    let _ = upon_proposal_1 t in
    (* L28 *)
    let _ = upon_proposal_2 t in
    (* L34 *)
    let _ = upon_prevote_once t in
    (* L36 *)
    let _ = upon_proposal_and_prevote t in
    (* L44 *)
    let _ = upon_nil_prevotes t in
    (* L47 *)
    let _ = upon_precommit_once t in
    (* L49 *)
    let _ = upon_proposal_and_precommit t in
    (* L55 *)
    let _ = upon_superior_rounds t in
    (* Custom *)
    let _ = upon_superior_height t in
    return ()

  (* Printing gossip *)

  let print_gossip t = function
    | Proposal (p, h, r, Some v, vr) ->
       if t.height = h && t.round = r then
         begin
           let ps = Crypto.public_key_to_string_hum p in
           let s = Printf.sprintf "[%s] Proposal [%d:%d] v = (#<block_data>,%d)" ps h r vr in
           ignore(Logging.print t.console s)
         end
    | Proposal (p, h, r, None, vr) ->
       if t.height = h && t.round = r then
         begin
           let ps = Crypto.public_key_to_string_hum p in       
           let s = Printf.sprintf "[%s] Proposal [%d:%d] v = (nil,%d)" ps h r vr in
           ignore(Logging.print t.console s)
         end
    | Prevote (p, h, r, Some v, signature) ->
       if t.height = h && t.round = r then
         begin
           let ps = Crypto.public_key_to_string_hum p in              
           let s = Printf.sprintf "[%s] Prevote [%d:%d] v = #<block_data>" ps h r in
           ignore(Logging.print t.console s)
         end
    | Prevote (p, h, r, None, signature) ->
       if t.height = h && t.round = r then
         begin
           let ps = Crypto.public_key_to_string_hum p in
           let s = Printf.sprintf "[%s] Prevote [%d:%d] v = nil" ps h r in
           ignore(Logging.print t.console s)
         end
    | Precommit (p, h, r, Some v, signature) ->
       if t.height = h && t.round = r then
         begin
           let ps = Crypto.public_key_to_string_hum p in       
           let s = Printf.sprintf "[%s] Precommit [%d:%d] v = #<block_data>" ps h r in
           ignore(Logging.print t.console s)
         end
    | Precommit (p, h, r, None, signature) ->
       if t.height = h && t.round = r then
         begin
           let ps = Crypto.public_key_to_string_hum p in
           let s = Printf.sprintf "[%s] Precommit [%d:%d] v = nil" ps h r in
           ignore(Logging.print t.console s)
         end
    | Sync (p, h, r) ->
       let ps = Crypto.public_key_to_string_hum p in       
       let s = Printf.sprintf "[%s] Sync [%d:%d]" ps h r in
       ignore(Logging.print t.console s)         
    | Bootstrap ->
       (* ignore(Logging.print t.console "Received bootstrap message") *)
       ()

  let update_state t u =
    List.iter u (fun (_, _, gossip) ->
        print_gossip t gossip;
        match gossip with
        | Proposal (p, h, r, v, vr) ->
           t.gossip <- GossipSet.add t.gossip (Proposal (p, h, r, v, vr))
        | Prevote (p, h, r, v, s) ->
           t.gossip <- GossipSet.add t.gossip (Prevote (p, h, r, v, s))
        | Precommit (p, h, r, v, s) ->
           t.gossip <- GossipSet.add t.gossip (Precommit (p, h, r, v, s))
        | Sync (p, h, r) ->
           if t.public_key <> p then
             begin
               let count = SyncAgent.set t.sync_agent p h r in
               ignore(Logging.print t.console (Printf.sprintf "--- SYNCHRONIZING(%d) ---" count));
             end
        | Bootstrap ->
           ()
      );
    trigger_rules t

end
