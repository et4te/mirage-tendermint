open Core_kernel

module Make
         (C : Mirage_console_lwt.S)
         (S : Mirage_stack_lwt.V4)
         (Pclock : Mirage_types.PCLOCK)
  = struct

  module Logging = Logging.Make(C)

  module Identity = Identity.Make(S)

  (* Types *)

  type timestamp = int * int64
    [@@deriving bin_io, sexp]

  type 'a t = (Identity.t * timestamp * 'a) list
    [@@deriving bin_io, sexp]

  let lim = ref 20

  (* Timestamp operations *)

  let is_newest (d1, ps1) (d2, ps2) =
    if d1 > d2 then
      1
    else if d1 = d2 then
      if ps1 > ps2 then
        1
      else if ps1 = ps2 then
        0
      else
        -1
    else
      -1

  (* Utility *)

  let compare_ts (d1, ps1) (d2, ps2) =
    is_newest (d1, ps1) (d2, ps2)

  let compare_id id1 id2 =
    if id1 > id2 then
      1
    else if id1 = id2 then
      0
    else
      -1

  let compare_combined_ts (_, (d1, ps1), _) (_, (d2, ps2), _) =
    compare_ts (d1, ps1) (d2, ps2)

  let compare_combined_id (id1, _, _) (id2, _, _) =
    compare_id id1 id2

  let compare_zipped_ts (_, (_, (d1, ps1), _)) (_, (_, (d2, ps2), _)) =
    compare_ts (d1, ps1) (d2, ps2)

  let compare_zipped_id (_, (id1, _, _)) (_, (id2, _, _)) =
    compare_id id1 id2

  (* Implementation *)

  let rec sample_cache_id t self_id =
    let i = Random.int (List.length t) in
    let (id, _, _) = List.nth_exn t i in
    if id = self_id then
      sample_cache_id t self_id
    else
      id

  (* O(N) worst-case, not ideal *)

  let update_id t (id, (d, ps), state) =
    let rec loop list accum = match list with
      | [] ->
         List.rev accum
      | hd :: tl ->
         if compare_combined_id hd (id, (d, ps), state) = 0 then
           loop tl ((id, (d, ps), state) :: accum)
         else
           loop tl (hd :: accum)
    in
    loop t []
           
  (* Deduplicates by id and keeps entries with freshest timestamps.
     When an entry belongs to the second list (`B), it is saved as 
     an update in a secondary accumulator.
   *)

  let dedup_freshest list =
    let rec loop list accum1 accum2 = match list with
      | [] ->
         accum1, accum2
      | (`A, hd) :: [] ->
         hd :: accum1, accum2
      | (`B, hd) :: [] ->
         hd :: accum1, hd :: accum2
      | hd1 :: hd2 :: tl ->
         if compare_zipped_id hd1 hd2 = 0 then 
           let r = compare_zipped_ts hd1 hd2 in
           if r = 1 then (** hd1 is newer (>) hd2 **)
             loop (hd1 :: tl) accum1 accum2
           else if r = 0 then
             loop (hd1 :: tl) accum1 accum2
           else 
             loop (hd2 :: tl) accum1 accum2
         else
           match hd1 with
           | (`A, hd) ->
              loop (hd2 :: tl) (hd :: accum1) accum2
           | (`B, hd) ->
              loop (hd2 :: tl) (hd :: accum1) (hd :: accum2)
    in
    loop list [] []

  (* Merge newest entries from cache t1 with cache t2 and return
     merged cache along with state updates
   *)

  let merge t1 t2 =
    let al = List.init (List.length t1) ~f:(fun _ -> `A) in
    let az = List.zip_exn al t1 in
    let bl = List.init (List.length t2) ~f:(fun _ -> `B) in
    let bz = List.zip_exn bl t2 in
    let r1, u1 =
      List.append az bz
      |> List.sort ~compare:compare_zipped_id
      |> dedup_freshest
    in
    let r2 = List.sort r1 ~compare:compare_combined_ts in
    let u2 = List.sort u1 ~compare:compare_combined_ts in
    List.take r2 !lim, u2

end
