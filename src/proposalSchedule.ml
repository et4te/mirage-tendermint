open Core_kernel
open Lwt

module Make
         (C : Mirage_console_lwt.S)
         (KV : Mirage_kv_lwt.RO)
  = struct

  module Logging = Logging.Make(C)

  type height = int
    [@@deriving bin_io, sexp]

  type round = int
    [@@deriving bin_io, sexp]

  type t = (height * round * Crypto.public_key) list
    [@@deriving bin_io, sexp]

  let proposer t h r =
    if r > 4 then
      begin
        let rm = r mod 4 in
        let (_, _, pk) = List.find_exn t ~f:(fun (h1, r1, _)  -> h1 = h && r1 = rm) in
        pk
      end
    else
      begin
        let (_, _, pk) = List.find_exn t ~f:(fun (h1, r1, _)  -> h1 = h && r1 = r) in
        pk
      end

  (* Serialize / Deserialize *)

  let serialize ps =
    ps |> sexp_of_t |> Sexp.to_string_hum

  let deserialize s =
    s |> Sexp.of_string |> t_of_sexp

  let of_cstruct cs =
    cs |> Cstruct.concat |> Cstruct.to_string |> deserialize

end
