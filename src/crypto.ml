open Core_kernel
open Hacl
open Hex

type hash = Bigstring.t [@@deriving bin_io, sexp]
type public_key = Bigstring.t [@@deriving bin_io, sexp]
type secret_key = Bigstring.t [@@deriving bin_io, sexp]
type signature = Bigstring.t [@@deriving bin_io, sexp]

(* Verifies a self-signed identity for testing *)
let verify public_key msg signature = 
  Sign.verify ~pk:(Sign.unsafe_pk_of_bytes public_key) ~msg ~signature

let sign secret_key msg =
  let sk = Sign.unsafe_sk_of_bytes secret_key in
  let signature = Bigstring.create 64 in
  ignore(Sign.sign ~sk ~msg ~signature);
  signature

let hash bs =
  let open Hash in
  let state = SHA256.init () in
  SHA256.update state bs;
  SHA256.finish state

(* Convert from hex encoded sexp to string *)
let bytes_of_sexp t =
  let h = t |> Sexp.to_string in
  (`Hex h) |> Hex.to_string |> Bigstring.of_string

(* Convert from pubkey bytes to hex encoded string *)
let sexp_of_bytes t =
  let (`Hex h) = t |> Bigstring.to_string |> Hex.of_string in
  h |> Sexp.of_string

let hash_of_sexp t = bytes_of_sexp t
let sexp_of_hash t = sexp_of_bytes t
let hash_to_string_hum t =
  t |> sexp_of_hash |> Sexp.to_string_hum

let public_key_of_sexp t = bytes_of_sexp t
let sexp_of_public_key t = sexp_of_bytes t
let public_key_to_string_hum t =
  t |> sexp_of_public_key |> Sexp.to_string_hum

let secret_key_of_sexp t = bytes_of_sexp t
let sexp_of_secret_key t = sexp_of_bytes t

let signature_of_sexp t = bytes_of_sexp t
let sexp_of_signature t = sexp_of_bytes t                             
