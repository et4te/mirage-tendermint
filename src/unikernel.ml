open Core_kernel
open Lwt

module Main
         (C : Mirage_console_lwt.S)
         (T : Mirage_time_lwt.S)
         (Mclock : Mirage_types.MCLOCK)
         (Pclock : Mirage_types.PCLOCK)
         (R : Mirage_random.S)
         (S : Mirage_stack_lwt.V4)
         (KV : Mirage_kv_lwt.RO)
  = struct

  module Logging = Logging.Make(C)

  module Settings = Settings.Make(C)(S)(KV)

  module ProposalSchedule = ProposalSchedule.Make(C)(KV)

  module Node = Node.Make(C)(T)(Pclock)(S)(KV)

  module Util = Util.Make(C)(Pclock)(S)(KV)

  let read_key kv key =
    KV.size kv key >>= function
    | Error e -> Lwt.return @@ Error e
    | Ok size -> KV.read kv key 0L size

  let read_file c kv name f =
    read_key kv name >|= function
    | Error e ->
       Logging.print_err c KV.pp_error e >>= fun () ->
       return None
    | Ok cs ->
       return (Some (f cs))

  let read_settings c kv name =
    read_file c kv name Settings.of_cstruct

  let read_schedule c kv name =
    read_file c kv name ProposalSchedule.of_cstruct

  let read_block c kv name =
    read_file c kv name Block.of_cstruct

  (* let start c t mc pc r s kv =
   *   ignore(Util.generate_test_network c pc s 4000 3000 7);
   *   S.listen s *)

  let start c t mc pc r s kv =
    let name = "node-" ^ (Key_gen.name ()) ^ ".sexp" in
    read_settings c kv name >>= fun settings ->
    match%lwt settings with
    | Some settings ->
       read_block c kv "genesis.sexp" >>= fun genesis ->
       (match%lwt genesis with
        | Some genesis ->
           read_schedule c kv "schedule.sexp" >>= fun schedule ->
           (match%lwt schedule with
            | Some schedule ->
               let node = Node.create c s kv pc settings schedule genesis in
               let () = Node.listen_udp node in
               let () = Node.listen_tcp node in
               S.listen s
            | None ->
               Logging.print c "[error] Invalid proposal schedule")
        | None ->
           Logging.print c "[error] Failed to read genesis")
    | None ->
       Logging.print c "[error] Invalid settings"

end
