open Core_kernel
open Hacl

module Make (S : Mirage_stack_lwt.V4) = struct

  module NetworkAddress = NetworkAddress.Make(S)

  type t = {
      public_key : Crypto.public_key;
      address : NetworkAddress.t;
    } [@@deriving bin_io, sexp]

  type s =
    | Unsigned of t
    | Signed of t * Crypto.signature
      [@@deriving bin_io, sexp]

  type k = Crypto.public_key * Crypto.secret_key
    [@@deriving bin_io, sexp]

  (* Creates an unsigned identity *)
  let create pk addr =
    Unsigned { public_key = pk; address = addr }

  (* Signs an identity *)
  let sign sk = function
    | Unsigned t ->
       let size = bin_size_t t in
       let msg = Bigstring.create size in
       let signature = Bigstring.create 64 in
       ignore(bin_write_t ~pos:0 msg t);
       ignore(Sign.sign ~sk ~msg ~signature);
       Signed (t, signature)
    | Signed (t, signature) ->
       Signed (t, signature)

  (* Verifies a self-signed identity for testing *)
  let verify = function
    | Unsigned t ->
       false
    | Signed (t, signature) ->
       let msg = Bigstring.create (bin_size_t t) in
       ignore(bin_write_t ~pos:0 msg t);
       Sign.verify ~pk:(Sign.unsafe_pk_of_bytes t.public_key) ~msg ~signature

  (* Generates a keypair & self-signed identity for testing *)

  let generate addr : k * s =
    let pk, sk = Sign.keypair () in
    let pk_bytes = Sign.unsafe_to_bytes pk in
    let sk_bytes = Sign.unsafe_to_bytes sk in
    (pk_bytes, sk_bytes), sign sk (create pk_bytes addr)

  exception Expected_signature
  exception Invalid_signature

  let of_signed_exn = function
    | Unsigned _ ->
       raise Expected_signature
    | Signed (t, signature) ->
       if verify (Signed (t, signature)) then
         t
       else
         raise Invalid_signature

end
