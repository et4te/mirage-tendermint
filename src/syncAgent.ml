open Core_kernel

let height_limit = 10
   
type t = {
    mutable height : int;
    messages : (int, Gossip.t list) Hashtbl.Poly.t;
    mutable sync : ((Crypto.public_key * int * int) option) list;
    mutable sync_queue : Gossip.t list;
  } [@@deriving bin_io, sexp]

let create () = {
    height = 0;
    messages = Hashtbl.Poly.create ();
    sync = [];
    sync_queue = [];
  }

let store t h1 message =
  let open Gossip in

  let rec clear_lower_heights i lim =
    if i >= 0 then
      if i >= lim then
        ()
      else
        begin
          Hashtbl.remove t.messages i;
          clear_lower_heights (i+1) lim
        end
    else
      ()
  in

  if h1 > t.height then
    begin
      clear_lower_heights (t.height - height_limit) (h1 - height_limit);
      t.height <- h1;
    end;
  
  match message with
  | Bootstrap ->
     ()
  | Sync (p, h, r) ->
     ()
  | Proposal (_, h, _, _, _) ->
     if h >= (h1 - height_limit) then
       (match Hashtbl.find t.messages h with
        | Some queue ->
           Hashtbl.set t.messages h (message :: queue)
        | None ->
           Hashtbl.add_exn t.messages h [message])
     else
       ()
  | Prevote (_, h, _, _, _) ->
     if h >= (h1 - height_limit) then
       (match Hashtbl.find t.messages h with
        | Some queue ->
           Hashtbl.set t.messages h (message :: queue)
        | None ->
           Hashtbl.add_exn t.messages h [message])
     else
       ()
  | Precommit (_, h, _, _, _) ->
     if h >= (h1 - height_limit) then
       (match Hashtbl.find t.messages h with
        | Some queue ->
           Hashtbl.set t.messages h (message :: queue)
        | None ->
           Hashtbl.add_exn t.messages h [message])
     else
       ()

let init_state () =
  None

(* Find all messages from height = h and round = r and set the
   sync_queue to these messages.
 *)

exception Too_far_behind

let build_sync_queue t h1 =
  if h1 < (t.height - height_limit) then
    raise Too_far_behind
  else
    begin
      let open Gossip in
      let rec collect q accum = match q with
        | [] ->
           accum
        | Proposal (p, h, r, v, vr) :: tl ->
           collect tl (Proposal (p, h, r, v, vr) :: accum)
        | Prevote (p, h, r, id, s) :: tl ->
           collect tl (Prevote (p, h, r, id, s) :: accum)
        | Precommit (p, h, r, id, s) :: tl ->
           collect tl (Precommit (p, h, r, id, s) :: accum)
        | hd :: tl ->
           collect tl accum
      in
      let rec loop h accum =
        match Hashtbl.find t.messages h with
        | Some q ->
           let hq = collect q [] in
           loop (h + 1) (List.append accum hq)
        | None ->
           accum
      in
      loop h1 []
    end

let status t =
  t.sync
  
let activate t pk h r =
  t.sync <- Some (pk, h, r) :: t.sync

let set t pk h r =
  t.sync <- Some (pk, h, r) :: t.sync;
  t.sync_queue <- build_sync_queue t h;
  List.length t.sync_queue

exception None_in_queue

let pop_head t =
  match t.sync with
  | [] ->
     ()
  | (Some (pk, h, r)) :: [] ->
     t.sync <- [];
     t.sync_queue <- build_sync_queue t h;
     ()
  | (Some (pk, h, r)) :: hd :: tl ->
     t.sync <- hd :: tl;
     t.sync_queue <- build_sync_queue t h;
     ()
  | None :: [] ->
     raise None_in_queue
  | None :: hd :: tl ->
     raise None_in_queue

let rec get_state t =
  match t.sync_queue with
  | [] ->
     pop_head t;
     get_state t
  | hd :: tl ->
     t.sync_queue <- tl;
     Some (hd)
