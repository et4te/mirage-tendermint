open Core_kernel

let block_reward = 1

type input = {
    previous_tx : Crypto.hash;
    output_idx : int;
    signature : Crypto.signature;
  } [@@deriving sexp, bin_io]

type output = {
    amount : int;
    pubkey_hash : Crypto.hash;
  } [@@deriving sexp, bin_io]

type t = {
    inputs : input list;
    outputs : output list;
  } [@@deriving sexp, bin_io]

let genesis_coinbase public_key quantity =
  let output = {
      amount = quantity;
      pubkey_hash = Crypto.hash public_key;
    } in
  { inputs = []; outputs = [output] }

let coinbase public_key =
  let output = {
      amount = block_reward;
      pubkey_hash = Crypto.hash public_key;
    } in
  { inputs = []; outputs = [output] }

let hash tx =
  let msg = Bigstring.create (bin_size_t tx) in
  ignore(bin_write_t ~pos:0 msg tx);
  Crypto.hash msg

exception Empty_list

let build_merkle_root txs = 
  let rec loop txs = match txs with
    | [] ->
       None
    | hd :: [] ->
       Some (hash hd)
    | hd1 :: hd2 :: [] ->
       let h = Bigstring.concat [hash hd1; hash hd2] in
       Some (Crypto.hash h)
    | list ->
       let pivot = List.length list / 2 in
       let l, r = List.split_n list pivot in
       let lh = loop l in
       let rh = loop r in
       match lh with
       | None ->
          (match rh with
           | None ->
              raise Empty_list
           | Some rh ->
              Some (rh))
       | Some lh ->
          (match rh with
           | None ->
              Some (lh)
           | Some rh ->
              Some (Crypto.hash (Bigstring.concat [lh; rh])))
  in
  match loop txs with
  | None ->
     raise Empty_list
  | Some h ->
     h
            
