open Core_kernel

type height = int
  [@@deriving bin_io, sexp]
type round = int
  [@@deriving bin_io, sexp]

type t =
  | Bootstrap
  | Sync of Crypto.public_key * height * round
  | Proposal of Crypto.public_key * height * round * Block.t option * int
  | Prevote of Crypto.public_key * height * round * Crypto.hash option * Crypto.signature option
  | Precommit of Crypto.public_key * height * round * Crypto.hash option * Crypto.signature option
    [@@deriving bin_io, sexp]

