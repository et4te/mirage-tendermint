open Mirage

let name =
  let doc = Key.Arg.info ~doc:"The node name." ["name"] in
  Key.(create "name" Arg.(opt string "" doc))

let disk = generic_kv_ro "priv"

let stack = generic_stackv4 default_network

let main =
  foreign
    ~keys:[ Key.abstract name ]
    ~packages:[
      package "duration";
      package "core_kernel";
      package "lwt_ppx";
      package "ppx_deriving";
      package "ppx_bin_prot";
      package "ppx_sexp_conv";
      package "ppx_compare";
      package "ppx_hash";
      package "ocamlgraph";
      package "blake2b_simd";
      package "hacl";
      package "hex";
    ]
    "Unikernel.Main" (console @-> time @-> mclock @-> pclock @-> random @-> stackv4 @-> kv_ro @-> job)

let () =
  register "mirage-tendermint" [
      main $ default_console $ default_time $ default_monotonic_clock $ default_posix_clock $ default_random $ stack $ disk;
  ]
