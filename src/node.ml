open Core_kernel
open Lwt

module Make
         (C : Mirage_console_lwt.S)
         (T : Mirage_time_lwt.S)
         (Pclock : Mirage_types.PCLOCK)
         (S : Mirage_stack_lwt.V4)
         (KV : Mirage_kv_lwt.RO)
  = struct

  module Util = Util.Make(C)(Pclock)(S)(KV)

  module Logging = Logging.Make(C)

  module NetworkAddress = NetworkAddress.Make(S)

  module Settings = Settings.Make(C)(S)(KV)

  module ProposalSchedule = ProposalSchedule.Make(C)(KV)

  module Identity = Identity.Make(S)

  module Cache = Cache.Make(C)(S)(Pclock)

  module Tendermint = Tendermint.Make(C)(T)(Pclock)(KV)

  (* Constants *)

  let network_size = 7
  let k = 7
  (** The number of nodes to exchange with per exchange **)

  (* Types *)

  type payload =
    | ExchangeReq of Gossip.t Cache.t
    | ExchangeRsp of Gossip.t Cache.t
      [@@deriving bin_io, sexp]

  type message = {
      id : Identity.s;
      payload : payload;
      req_no : int;
    } [@@deriving bin_io, sexp]

  type consensus_state = Waiting | Running

  type t = {
      net : S.t;
      console : C.t;
      kv : KV.t;
      pclock : Pclock.t;
      settings : Settings.t;
      id : Identity.t;
      udp_port : int;
      tcp_port : int;
      mutable cache : Gossip.t Cache.t;
      exchange_requests : (int, unit Lwt_condition.t) Hashtbl.Poly.t;
      mutable exchange_count : int;
      mutable peer_list : Identity.t list;
      mutable request_count : int;
      tendermint : Tendermint.t;
      mutable consensus : consensus_state;
    }

  (* Implementation *)

  let create c s kv pclock settings schedule genesis =
    let open Settings in
    let open Identity in
    let id = Identity.of_signed_exn settings.identity in
    { net = s;
      console = c;
      kv;
      pclock;
      settings;
      id;
      udp_port = id.address.udp_port;
      tcp_port = id.address.tcp_port;
      cache = [];
      exchange_requests = Hashtbl.Poly.create ();
      exchange_count = 0;
      peer_list = [];
      request_count = 0;
      tendermint = Tendermint.create c pclock id.public_key settings.secret_key schedule genesis;
      consensus = Waiting;
    }

  exception Empty_peer_list

  let sample_id t self_id =
    if List.length t.peer_list < (network_size - 1) then
      Cache.sample_cache_id t.cache self_id
    else
      match t.peer_list with
      | [] ->
         raise Empty_peer_list
      | peer_id::tl ->
         t.peer_list <- List.append tl [peer_id];
         peer_id

  let next_req_no t =
    let req_no = t.request_count in
    t.request_count <- t.request_count + 1;
    req_no

  let next_exchange_count t =
    let exch_no = t.exchange_count in
    t.exchange_count <- t.exchange_count + 1;
    exch_no

  let send t ~id ~payload ~req_no =
    let open Identity in
    let self_id = Settings.get_identity t.settings in
    let msg : message = { id = self_id; payload; req_no } in
    let size = bin_size_message msg in
    let buf = Cstruct.create size in
    ignore(bin_write_message ~pos:0 (Cstruct.to_bigarray buf) msg);
    let udp = S.udpv4 t.net in
    S.UDPV4.write ~src_port:t.udp_port ~dst:id.address.ip ~dst_port:id.address.udp_port udp buf >>= function
    | Ok () ->
       return ()
    | Error err ->
       (* TODO: print_err *)
       return ()

  let wait t req_no =
    let cond = Hashtbl.find_or_add t.exchange_requests req_no ~default:Lwt_condition.create in
    Lwt_condition.wait cond
    
  let wait_timeout t req_no timeout =
    pick [
        (T.sleep_ns (Int64.of_int timeout) >>= fun () -> (return `Timeout));
        (wait t req_no >>= fun () -> (return `Ok))
      ]

  let initiate_exchange t id =
    let req_no = next_req_no t in
    send t ~id ~payload:(ExchangeReq t.cache) ~req_no >>= fun () ->
    wait_timeout t req_no (Settings.get_round_trip_time t.settings)

  let initiate_custom_exchange t id cache =
    let req_no = next_req_no t in
    send t ~id ~payload:(ExchangeReq cache) ~req_no >>= fun () ->
    wait_timeout t req_no (Settings.get_round_trip_time t.settings)

  let finalize_exchange t id req_no =
    send t ~id ~payload:(ExchangeRsp t.cache) ~req_no

  let rec bootstrap t =
    let open Gossip in
    (* Create init agent state *)
    let d, ps = Pclock.now_d_ps t.pclock in
    t.cache <- [(t.id, (d, ps), Bootstrap)];
    (* Initiate bootstrap *)
    let id = Settings.random_bootstrap_id t.settings in
    match%lwt initiate_exchange t id with
    | `Timeout ->
       (* ignore(Logging.print t.console "[timeout] Bootstrap timeout, waiting ..."); *)
       T.sleep_ns (Int64.of_int t.settings.refresh_period) >>= fun () ->
       bootstrap t
    | `Ok ->
       return ()

  (* Fetch latest agent state, update self_id in t.cache *)
  let update_agent_state t =
    let d, ps = Pclock.now_d_ps t.pclock in
    let state = Tendermint.get_state t.tendermint in
    match state with
    | None ->
       return ()
    | Some s ->
       let entry = (t.id, (d, ps), s) in
       let cache = Cache.update_id t.cache entry in
       t.cache <- cache;
       Tendermint.update_state t.tendermint [entry]

  (* k peers exchanged with, if refresh_rate = 1s and k = 3 then this probes 3
     peers per second round-robin. With a network of n = 7 that is k * 7 = 21
     exchange requests per second on the network (3 exchanges per node).
   *)
  let exchange t =
    update_agent_state t >>= fun () ->
    let saved_cache = t.cache in
    for i = 0 to k do
      let id = sample_id t t.id in
      async(fun () ->
          match%lwt initiate_custom_exchange t id saved_cache with
          | `Timeout ->
             (* ignore(Logging.print t.console "[timeout] Exchange request timed out ..."); *)
             Lwt.return ()
          | `Ok ->
             Lwt.return ()
        )
    done;
    Lwt.return ()

  let merge_state t id entries =
    let cache, updates = Cache.merge t.cache entries in
    ignore(Tendermint.update_state t.tendermint updates);
    t.cache <- cache

  let rec refresh_loop t =
    let refresh_period = Int64.of_int t.settings.refresh_period in
    (if (List.length t.cache) = 0 then
       begin
         bootstrap t >>= fun () ->
         T.sleep_ns refresh_period
       end
     else
       begin
         (if (t.consensus = Waiting) && (List.length t.peer_list >= (network_size - 1)) then
            begin
              ignore(async (fun () ->
                         let () = Tendermint.start t.tendermint in
                         return ()
                ));
              t.consensus <- Running;
            end);
         join [
             exchange t >>= fun () ->
             T.sleep_ns refresh_period
           ]
       end
    ) >>= fun () ->
    refresh_loop t

  let handle_message t msg =
    Lwt_result.return ()

  let handle_exchange t msg =
    match msg.payload with
    | ExchangeReq entries ->
       let id = Identity.of_signed_exn msg.id in
       if not (List.mem t.peer_list id ~equal:(fun x y -> x = y)) then
         t.peer_list <- List.append t.peer_list [id];
       finalize_exchange t id msg.req_no >>= fun () ->
       merge_state t id entries;
       Lwt_result.return ()
    | ExchangeRsp entries ->
       match Hashtbl.find t.exchange_requests msg.req_no with
       | Some cond ->
          let () = Lwt_condition.broadcast cond () in
          let id = Identity.of_signed_exn msg.id in
          merge_state t id entries;
          Lwt_result.return ()
       | None ->
          (* Potentially malicious *)
          Lwt_result.return ()

  let callback_udp t ~src ~dst ~src_port buf =
    let size = Cstruct.len buf in
    let msg = bin_read_message (Cstruct.to_bigarray buf) (ref 0) in
    let%lwt _ = handle_exchange t msg in
    return ()

  let listen_udp t =
    async (fun () -> refresh_loop t);
    ignore(Logging.print t.console (Printf.sprintf "[udp] Listening on %d" t.udp_port));
    S.listen_udpv4 t.net ~port:t.udp_port (callback_udp t)

  (* not implemented *)

  let callback_tcp t tcp_flow =
    return ()

  let listen_tcp t =
    ignore(Logging.print t.console (Printf.sprintf "[tcp] Listening on %d" t.tcp_port));
    S.listen_tcpv4 t.net ~port:t.tcp_port (callback_tcp t)

end
