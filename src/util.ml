open Core_kernel
open Lwt

module Make
         (C : Mirage_console_lwt.S)
         (Pclock : Mirage_types.PCLOCK)
         (S : Mirage_stack_lwt.V4)
         (KV : Mirage_kv_lwt.RO)
  = struct

  module Logging = Logging.Make(C)

  module NetworkAddress = NetworkAddress.Make(S)

  module Identity = Identity.Make(S)

  module Settings = Settings.Make(C)(S)(KV)

  module ProposalSchedule = ProposalSchedule.Make(C)(KV)

  let get_ip stack =
    List.hd_exn (S.IPV4.get_ip (S.ipv4 stack))

  let generate_network_settings c s udp_port_start tcp_port_start lim =
    let open Settings in
    let open NetworkAddress in
    let rec loop ip udp_port tcp_port acc participants =
      if (udp_port >= (udp_port_start + lim)) || (tcp_port >= (tcp_port_start + lim)) then
        acc, participants
      else
        begin
          let addr = { ip; udp_port; tcp_port } in
          let (pk, sk), identity = Identity.generate addr in
          let settings : Settings.t = {
              identity;
              secret_key = sk;
              bootstrap_peers = [];
              refresh_period = 50000000;
              round_trip_time = 5000000;
            } in
          loop ip (udp_port + 1) (tcp_port + 1) (settings :: acc) (pk :: participants)
        end
    in
    loop (get_ip s) udp_port_start tcp_port_start [] []

  let print_network_settings c settings_list =
    let rec loop settings_list = match settings_list with
      | [] ->
         return ()
      | hd :: tl ->
         let settings_s = Settings.serialize hd in
         Logging.print c (Printf.sprintf "settings\n%s\n" settings_s) >>= fun () ->
         loop tl
    in
    loop settings_list

  exception No_elements

  let rec random_id_exn l self =
    if (List.length l) > 0 then
      begin
        let i = Random.int (List.length l) in
        let p = List.nth_exn l i in
        if p = self then
          random_id_exn l self
        else
          p
      end
    else
      raise No_elements

  let add_bootstrap_node settings node_ids =
    let open Settings in 
    { settings with bootstrap_peers = [random_id_exn node_ids settings.identity] }

  let add_bootstrap_nodes settings_list =
    let open Settings in
    let node_ids = List.map settings_list (fun settings -> settings.identity) in
    List.map settings_list (fun settings -> add_bootstrap_node settings node_ids)

  let generate_proposal_schedule height_lim round_lim participants =
    let rotate_participants ps =
      let p = List.hd_exn ps in
      p, List.append (List.tl_exn ps) [p]
    in
    let rec loop_rounds participants height round rounds =
      if round >= round_lim then
        rounds
      else
        begin
          let p, ps = rotate_participants participants in
          loop_rounds ps height (round+1) (List.append [(height, round, p)] rounds)
        end
    in      
    let rec loop_height participants height schedule =
      if height >= height_lim then
        schedule
      else
        begin
          let rounds = loop_rounds participants height 0 [] in
          let p, ps = rotate_participants participants in
          loop_height ps (height+1) (List.append schedule rounds)
        end
    in
    loop_height participants 0 []

  let print_schedule c schedule =
    let schedule_s = ProposalSchedule.serialize schedule in
    Logging.print c (Printf.sprintf "schedule:\n%s\n" schedule_s)

  let print_genesis c genesis =
    let genesis_s = Block.serialize genesis in
    Logging.print c (Printf.sprintf "genesis:\n%s\n" genesis_s)

  (* Test *)

  let generate_test_network c pc s udp_port_start tcp_port_start port_lim =
    let s1, pks = generate_network_settings c s udp_port_start tcp_port_start port_lim in
    let s2 = add_bootstrap_nodes s1 in
    ignore(print_network_settings c s2);
    let schedule = generate_proposal_schedule 10000 5 pks in
    ignore(print_schedule c schedule);
    let (d, ps) = Pclock.now_d_ps pc in
    let genesis = Block.generate_genesis pks (d, ps) 1000000 in
    ignore(print_genesis c genesis)

end
