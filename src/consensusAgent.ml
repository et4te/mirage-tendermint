open Core_kernel

type t = {
    mutable queue : Gossip.t list;
  } [@@deriving bin_io, sexp]

let create () = {
    queue = [];
  }

let init_state () =
  None

let broadcast t message =
  t.queue <- message :: t.queue

let get_state t =
  match t.queue with
  | [] ->
     None
  | hd :: tl ->
     t.queue <- tl;
     Some (hd)
