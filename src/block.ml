open Core_kernel

type timestamp = int * int64

type t = {
    previous_hash : Crypto.hash option;
    merkle_root : Crypto.hash;
    transactions : Transaction.t list;
    timestamp : int * int64;
  } [@@deriving sexp, bin_io]

let hash block =
  let msg = Bigstring.create (bin_size_t block) in
  ignore(bin_write_t ~pos:0 msg block);
  Crypto.hash msg

exception Invalid_block

let get_block_exn = function
  | Some block ->
     block
  | None ->
     raise Invalid_block

(* Serialize / Deserialize *)

let serialize b =
  sexp_of_t b |> Sexp.to_string_hum

let deserialize s =
  s |> Sexp.of_string |> t_of_sexp

let of_cstruct cs =
  cs |> Cstruct.concat |> Cstruct.to_string |> deserialize

(* Implementation *)

let generate_genesis_txs pk_list quantity =
  let rec loop pk_list accum = match pk_list with
    | [] ->
       accum
    | pk :: tl ->
       loop tl (Transaction.genesis_coinbase pk quantity :: accum)
  in
  loop pk_list []

let generate_genesis pk_list timestamp quantity =
  let txs = generate_genesis_txs pk_list quantity in
  { previous_hash = None;
    merkle_root = Transaction.build_merkle_root txs;
    transactions = txs;
    timestamp;
  }

(* Generate a valid block *)

let generate_valid public_key prev_block txs timestamp =
  let msg = Bigstring.create (bin_size_t prev_block) in
  ignore(bin_write_t ~pos:0 msg prev_block);
  let previous_hash = Some (Crypto.hash msg) in
  let complete_txs = Transaction.coinbase public_key :: txs in
  { previous_hash;
    merkle_root = Transaction.build_merkle_root complete_txs;
    transactions = complete_txs;
    timestamp;
  }

exception Invalid_timestamp
exception Invalid_merkle_root
exception Invalid_previous_block
exception Invalid_coinbase

let verify_block p (prev_block : t) (block : t) =
  let prev_block_hash = Some (hash prev_block) in
  let merkle_root = Transaction.build_merkle_root block.transactions in
  if List.hd_exn (block.transactions) = Transaction.coinbase p then
    if prev_block_hash = block.previous_hash then
      if merkle_root = block.merkle_root then
        if block.timestamp > prev_block.timestamp then
          true
        else
          raise Invalid_timestamp
      else
        raise Invalid_merkle_root
    else
      raise Invalid_previous_block
  else
    false

let valid p prev_block = function
  | Some block ->
     verify_block p prev_block block
  | None ->
     false

