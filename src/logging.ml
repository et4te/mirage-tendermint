open Core_kernel
open Lwt

module Make (C : Mirage_console_lwt.S) = struct

  type t = C.t

  let print t s =
    C.log t s

  let print_err t ppf e =
    C.log t (Fmt.kstrf (fun x -> x) "%a" ppf e)

  let print_inet t fmt msg_sexp addr_sexp =
    let msg_s = Sexp.to_string_hum msg_sexp in
    let sender_s = Sexp.to_string_hum addr_sexp in
    print t (Printf.sprintf fmt sender_s msg_s)

end
