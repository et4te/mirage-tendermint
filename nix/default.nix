{ pkgs, stdenv, opam2nix }:

let
  blake2b_simd = pkgs.callPackage (pkgs.fetchgit {
    url = https://gitlab.com/et4te/ocaml-blake2b-simd;
    sha256 = "04mifq5bf6s48kf76rs408pfxqzklkw4k6b07q01bp6k2hdwn1p3";
  }) {};
  hacl = pkgs.callPackage (pkgs.fetchgit {
    url = https://gitlab.com/et4te/ocaml-hacl;
    sha256 = "0yf4h96cz8287j4hrvpqnkw0rwzqkhasj8byyiss3big0cd0j03r";
  }) {};
in

stdenv.mkDerivation {
  name = "mirage-tendermint";
  src = ../.;
  buildInputs = opam2nix.build {
    specs = opam2nix.toSpecs [
      "bigstring"
      "core_kernel"
      "crunch"
      "hex"
      "lwt"
      "lwt_ppx"
      "mirage"
      "mirage-logs"
      "mirage-unix"
      "mirage-clock-unix"
      "mirage-clock-lwt"      
      "mirage-console-unix"
      "mirage-fs-unix"
      "mirage-net-unix"
      "mirage-types-lwt"
      "ocamlbuild"
      "ocamlfind"
      "ocamlgraph"
      "ppx_deriving"
      "ppx_bin_prot"
      "ppx_sexp_conv"
      "ppx_compare"
      "ppx_hash"
      "tcpip"
      "zarith"
    ];
    ocamlAttr = "ocaml";
    ocamlVersion = "4.06.1";
  };
  OCAMLPATH="${hacl}/lib/hacl/install/default/lib:${blake2b_simd}/lib/blake2b_simd/install/default/lib";
  buildPhase = ''
    cd ./src
    mirage configure --net socket
    mirage build
  '';
  installPhase = ''
    mkdir $out/bin
    cp --dereference main.native $out/bin/mirage-tendermint
  '';
}
